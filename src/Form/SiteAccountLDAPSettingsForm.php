<?php

namespace Drupal\site_account_ldap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SiteAccountLDAPSettingsForm.
 */
class SiteAccountLDAPSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_account_ldap_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_account_ldap.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Загружаем конфигурацию.
    $config = $this->config('site_account_ldap.settings');

    // Сервер LDAP.
    $form['ldap_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LDAP server name'),
      '#default_value' => $config->get('ldap_url') ? $config->get('ldap_url') : "192.168.1.1",
      '#description' => '',
    ];

    // Название домена сервера.
    $form['ldap_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain name'),
      '#default_value' => $config->get('ldap_domain') ? $config->get('ldap_domain') : "site",
      '#description' => '',
    ];

    // Название зоны.
    $form['ldap_zone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zone name'),
      '#default_value' => $config->get('ldap_zone') ? $config->get('ldap_zone') : "ru",
      '#description' => '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Записывает значения в конфигурацию.
    $config = \Drupal::service('config.factory')->getEditable('site_account_ldap.settings');
    $config->set('ldap_url', trim($form_state->getValue('ldap_url')));
    $config->set('ldap_domain', trim($form_state->getValue('ldap_domain')));
    $config->set('ldap_zone', trim($form_state->getValue('ldap_zone')));

    $config->save();
  }
}